import asyncio
from proxybroker import Broker

async def show(proxies):
    r = []
    while True:
        proxy = await proxies.get()
        if proxy is None: break
        r.append(proxy)
    return r

def get(limit=10):
    proxies = asyncio.Queue(maxsize=limit)
    broker = Broker(proxies)
    return asyncio.get_event_loop().run_until_complete(asyncio.gather(broker.find(types=['CONNECT:80'], countries=["US"], limit=limit), show(proxies)))[1]

if __name__ == "__main__":
    for proxy in get():
        print(proxy)
