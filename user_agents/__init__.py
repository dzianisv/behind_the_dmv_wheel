import json
import glob
import os

agents = []

for file in glob.glob(os.path.join(os.path.dirname(__file__), "*.json")):
    with open(file, "r") as f:
        agents.extend(json.load(f))
