# About
If you have to go to the DMV in the San Francisco Bay Area, it can take weeks or even months to find an appointment. 
But people sometimes cancel those appointments!
Those cancelled appointments could be yours, if you'd like to click on the DMV site all day hoping to get one.

This bot bypasses recaptcha using "Chromium Buster: Captcha Solver for Humans" plugin and register a behind the wheel appointment at DMV offices you want.


## Installation

```bash
apt update -yq && apt install -yq chromium-chromedriver python3 python3-pip
pip3 install -r requirements
```

## Configuration
```sh
cp config.json.sample config.json
vi config.json
```

# Disclaimer

PLEASE DON'T ABUSE THIS PROGRAM. USE IT PROPERLY AND LEAVE APPOINTMENTS TO PEOPLE WHO ALSO NEED THEM. 
DO NOT SCHEDULE TO MANY APPOINTMENTS.
CANCEL ADDITIONAL APPOINTMENTS IF YOU FINISH YOUR STAFF AT DMV.
