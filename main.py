from selenium import webdriver
import config
from datetime import datetime, date
import time
import sys
import os
import random
import threading
import logging
import argparse
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException, TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
import user_agents
from enum import Enum
import proxy
import argparse

DATES_FILE = os.environ.get("DATES_FILE", "register.log")

class State(Enum):
    NULL = -1
    BEGIN = 0
    FORM = 1
    SUBMIT = 2
    ERROR = 3
    SOLVING = 4
    CHECK = 5
    DONE = 6

MAX_PUZZLES = 16

class Appointment(object):
    def __init__(self, location, proxies=None):
        self.location = location
        self.proxies = proxies
        self.state = State.NULL
        self.logger = logging.getLogger(location)
        self.logger.setLevel(logging.DEBUG)
        self.captcha_puzzles = 0

        log_handler = logging.StreamHandler()
        log_handler.setFormatter(logging.Formatter("%(asctime)s %(name)s %(message)s"))
        self.logger.addHandler(log_handler)
        self.browser = None

    def run_state(self):
        while True:
            self.logger.debug("%s", self.state)

            if self.state == State.NULL:
                self.captcha_puzzles = 0

                if self.browser:
                    self.browser.quit()

                user_agent = random.choice(user_agents.agents)
                options = webdriver.ChromeOptions()
                options.add_extension('./buster: Captcha Solver for Humans.crx')
                options.add_argument(f"user-agent={user_agent}")
                # options.add_argument("--headless")
                self.logger.debug("creating a new browser session \"%s\"", user_agent)
                if self.proxies:
                    proxy = random.choice(self.proxies)
                    self.logger.debug("using %s:%d", proxy.host, proxy.port)
                    options.add_argument(f"--proxy-server={proxy.host}:{proxy.port}")

                self.browser = webdriver.Chrome(options=options)
                self.state = State.BEGIN
                continue

            elif self.state == State.BEGIN:
                self.browser.get("https://www.dmv.ca.gov/wasapp/foa/driveTest.do")
                self.state = State.FORM
                continue

            elif self.state == State.FORM:
                WebDriverWait(self.browser, 10).until(expected_conditions.element_to_be_clickable((By.XPATH, '//*[@id="findOffice"]/fieldset/div[5]/input[2]')))

                office = self.browser.find_element_by_id("officeId")
                Select(office).select_by_visible_text(self.location)

                if config.testType == "auto":
                    self.browser.find_element_by_id("DT").click()
                elif config.testType == "motor":
                    self.browser.find_element_by_id("MC").click()
                    self.browser.find_element_by_id("yes").click()
                else:
                    raise Exception("Invalid configuration option")

                self.browser.find_element_by_id("firstName").send_keys(config.firstName)
                self.browser.find_element_by_id("lastName").send_keys(config.lastName)
                self.browser.find_element_by_id("dl_number").send_keys(config.ID)
                self.browser.find_element_by_id("birthMonth").send_keys(config.birthMonth)
                self.browser.find_element_by_id("birthDay").send_keys(config.birthDay)
                self.browser.find_element_by_id("birthYear").send_keys(config.birthYear)
                self.browser.find_element_by_id("areaCode").send_keys(config.phonenum1)
                self.browser.find_element_by_id("telPrefix").send_keys(config.phonenum1)
                self.browser.find_element_by_id("telSuffix").send_keys(config.phonenum1)

                self.state = State.SUBMIT
                continue

            elif self.state == State.SUBMIT:
                self.browser.find_element_by_xpath('//*[@id="findOffice"]/fieldset/div[5]/input[2]').click()
                self.state = State.CHECK
                continue

            elif self.state == State.ERROR:
                try:
                    self.browser.switch_to.frame(self.browser.find_element_by_xpath('//iframe[@title="recaptcha challenge"]'))
                    solver_button = self.browser.find_element_by_id('solver-button')

                    self.captcha_puzzles += 1
                    if self.captcha_puzzles > MAX_PUZZLES:
                        self.logger.error("captcha solver failed")
                        self.state = State.NULL
                        continue

                    solver_button.click()
                    self.state = State.SOLVING
                    continue
                except Exception:
                    self.logger.debug("solver-button is not found")
                finally:
                    self.browser.switch_to.default_content()

                try:
                    self.browser.switch_to.frame(self.browser.find_element_by_xpath('//iframe[@title="recaptcha challenge"]'))
                    self.browser.find_element_by_id('reset-button').click()
                    # reset state, recreate browser with a new user agent and proxy server
                    self.logger.warning("reCaptcha blocked current browser session, reseting...")
                    self.state = State.NULL
                    continue
                except Exception:
                    self.logger.debug("reset-button is not found")
                finally:
                    self.browser.switch_to.default_content()

                try:
                    self.browser.find_element_by_xpath('//*[@id="app_header"]/h1[text()="Session Timeout]')
                    self.state = State.BEGIN
                    continue
                except NoSuchElementException:
                    pass

                self.state = State.CHECK
                continue

            elif self.state == State.SOLVING:
                try:
                    WebDriverWait(self.browser, random.randint(3, 10)).until(expected_conditions.invisibility_of_element((By.XPATH, '//iframe[@title="recaptcha challenge"]')))
                    self.state = State.CHECK
                    continue
                except TimeoutException:
                    self.state = State.ERROR
                    continue

            elif self.state == State.CHECK:
                try:
                    self.browser.find_element_by_xpath('//*[@id="formId_1"]/div/div[2]/table/tbody/tr/td[1]')
                except NoSuchElementException:
                    self.logger.warning("Schedule is not found")
                    self.state = State.ERROR
                    continue

                try:
                    strdate_list = [line.strip() for line in open(DATES_FILE, 'r')] if os.path.exists(DATES_FILE) else []  # Converts dateLog.txt file to string list
                    # Converts string list to date list
                    date_list = convertDateStr(strdate_list)

                    if len(date_list) > 0:  # Empty date list = first run = no closest appointment
                        closest = min(date_list)

                    tempDate = datetime.strptime(
                        self.browser.find_element_by_xpath(
                            '//*[@id="formId_1"]/div/div[2]/table/tbody/tr/td[2]/p[2]/strong').text,
                        '%b %d, %Y at %I:%M %p').date()
                    if dateFitsPreferences(tempDate):
                        if determineRepeated(tempDate, date_list):
                            pass  # Ignore date because repeated
                        else:
                            self.logger.info('New entry found! ////////////////////' + str(tempDate))
                            dateLog = open(DATES_FILE, "a")
                            dateLog.write(str(tempDate) + "\n")
                            dateLog.flush()  # dateLog updated via text file
                            # dateLog also updated manually through append
                            date_list.append(tempDate)
                            newclosest = min(date_list)  # no min
                            if len(date_list) == 1:
                                # (0) occurs on first run, no previous appointment to cancel
                                self.register(0)
                            else:
                                if closest != newclosest:
                                    # (1) occurs after first run, cancel previous appointment
                                    self.register(1)
                    # "Refresh" Search
                    Select(self.browser.find_element_by_id("requestedTime")).select_by_visible_text('8:00 AM')
                    self.browser.find_element_by_id("checkAvail").click()

                except NoSuchElementException:
                    pass


            elif self.state == State.DONE:
                pass

            break
    # Automates appointment booking after new closest appointment meets requirements
    def register(self, num):
        self.browser.find_element_by_xpath('//*[@id="app_content"]/div/a[1]').click()
        # No "cancel previous appointment" if running for the first time
        self.logger.info("Booking appointment...")
        try:
            self.logger.info("Cancelling previous appointment...")
            # Clicks "cancel previous appointment"
            self.browser.find_element_by_xpath('//*[@id="ApptForm"]/button').click()
        except NoSuchElementException:
            self.logger.warning("First run!")
        self.browser.find_element_by_xpath('//*[@id="telephone_method"]').click()
        self.browser.find_element_by_xpath(
            '//*[@id="ApptForm"]/fieldset/div[11]/button').click()
        self.browser.find_element_by_xpath(
            '// *[ @ id = "ApptForm"] / fieldset / div[9] / button').click()
        scrollTo = self.browser.ser.find_element_by_xpath(
            '//*[@id="app_content"]/div/p[1]/strong')
        self.browser.execute_script("arguments[0].scrollIntoView();", scrollTo)
        self.browser.save_screenshot(f"{datetime.today()}.png")
        self.state = State.DONE

    def __del__(self):
        if self.browser:
            self.browser.quit()

# Checks if tempDate can already be found in date_list. If it is found, tempDate will be ignored
def determineRepeated(tempDate, date_list):
    if len(date_list) == 0:
        return False
    else:
        for i in range(len(date_list)):
            if str(tempDate) == str(date_list[i]):
                return True
            else:
                continue
        return False

# Converts the str_datelist parsed from dateLog.txt to a list of date objects
def convertDateStr(strdate_list):
    date_list = []
    for i in range(len(strdate_list)):
        date = datetime.strptime(strdate_list[i], '%Y-%m-%d').date()
        date_list.append(date)
    return date_list

# Checks to see if appointment date falls in desiredDateRange in config.py
def dateFitsPreferences(tempDate):
    if config.earliestDate <= tempDate <= config.latestDate:
        return True
    else:
        return False

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--use-proxy', action="store_true")
    args = parser.parse_args()

    event = threading.Event()
    workers = []
    proxies = proxy.get(len(config.locations) * 2) if args.use_proxy else  None

    for location in config.locations:
        appointment = Appointment(location, proxies)
        appointment.run_state()

        def update_loop():
            while not event.wait(config.refreshInterval):
                appointment.run_state()

        worker = threading.Thread(target=update_loop)
        worker.start()
        workers.append(worker)

    try:
        event.wait()
    except Exception:
        event.set()
    finally:
        for worker in workers:
            worker.join()

if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    main()