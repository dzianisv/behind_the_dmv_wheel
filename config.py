from datetime import datetime
import json
import os

locations = ['REDWOOD CITY', 'SAN MATEO', 'FREMONT', 'SANTA CLARA', 'SAN JOSE', 'SAN JOSE', 'SAN JOSE DLPC', 'HAYWARD', 'GILROY', 'ROCKLIN']  # Office ID exactly how it appears after selected from dropdown
testType = 'auto'  # 'auto' or 'motor'.
firstName = None
lastName = None
ID = None
birthMonth = None
birthDay = None
birthYear = None
phonenum1 = None
refreshInterval = 45  # in seconds

#Specify dates to exclude from appointment search
earliestDate = datetime(1954, 1, 1).date() #formatted year, month, day. No preference => set to = date.today()
latestDate = datetime(9999, 12, 31).date() #No preference => set to = datetime(9999, 12, 31).date()

with open(os.environ.get("CONFIG", "config.json"), 'r') as f:
    obj = json.load(f)
    locations = obj.get("locations")
    testType = obj.get("testType")
    firstName = obj.get("firstName")
    lastName = obj.get("lastName")
    ID = obj.get("ID")
    phonenum1 = obj.get("phone")
    birthMonth, birthDay, birthYear = obj.get("birthday").split("-")
